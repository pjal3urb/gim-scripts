if GameInfoManager and GameInfoManager.plugin_active("units") then
	
	local default_friendly_color = ContourExt._types.friendly.color
	ContourExt._types.friendly.color = nil
	
	local add_original = ContourExt.add
	
	function ContourExt:add(type, ...)
		local result = add_original(self, type, ...)
		
		if result and type == "friendly" then
			self:change_color("friendly", default_friendly_color)
		end
		
		return result
	end
	
	GameInfoManager.add_post_init_event(function()
		local function joker_event(event, key, data)
			if data.owner then
				managers.gameinfo:add_scheduled_callback(key .. "_joker_contour", 0.01, function()
					if alive(data.unit) and data.unit:contour() then
						data.unit:contour():change_color("friendly", tweak_data.chat_colors[data.owner] or default_friendly_color)
					end
				end)
			end
		end
		
		managers.gameinfo:register_listener("joker_contour_listener", "minion", "set_owner", joker_event)
	end)
	
else
	log("Warning: JokerContours script requires GameInfoManager with the Units plugin loaded to work")
end



