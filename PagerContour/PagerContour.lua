local SYNC_CONTOUR = false

if GameInfoManager and GameInfoManager.plugin_active("pagers") then
	GameInfoManager.add_post_init_event(function()
		local function pager_event(event, key, data)
			if alive(data.unit) and data.unit:contour() then
				if event == "set_answered" then
					data.unit:contour():add("medic_heal", SYNC_CONTOUR, 6)
				else
					data.unit:contour():remove("medic_heal", SYNC_CONTOUR)
				end
			end
		end
	
		managers.gameinfo:register_listener("pager_contour_remover", "pager", "set_answered", pager_event)
		managers.gameinfo:register_listener("pager_contour_remover", "pager", "remove", pager_event)
	end)
else
	log("Warning: PagerCountour script requires GameInfoManager with the Pager plugin loaded to work")
end